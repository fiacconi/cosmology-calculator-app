package com.fiacconi.davide.cosmologycalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.content.Intent;


public class CosmoActivity extends AppCompatActivity {

    /** ATTRIBUTES **/
    private TextView hubble_text;
    private TextView matter_text;
    private TextView lambda_text;

    private SeekBar hubble_bar;
    private SeekBar matter_bar;
    private SeekBar lambda_bar;

    private CheckBox flat_check;

    private Button run_button;
    private Button clear_button;

    private ImageButton plus_button;
    private ImageButton minus_button;

    private EditText redshift_input;

    private double HubbleConstant = 69.0;
    private double OmegaMatter = 0.3;
    private double OmegaLambda = 0.7;
    private double Redshift = 2.0;
    private boolean FlatUniverse;

    private CosmoCalc CosmoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cosmo);

        /** Initialize the variables for the attributes **/
        hubble_text = (TextView)findViewById(R.id.hubble_text);
        matter_text = (TextView)findViewById(R.id.matter_text);
        lambda_text = (TextView)findViewById(R.id.lambda_text);

        hubble_bar = (SeekBar)findViewById(R.id.hubble_seekBar);
        matter_bar = (SeekBar)findViewById(R.id.matter_seekBar);
        lambda_bar = (SeekBar)findViewById(R.id.lambda_seekBar);

        redshift_input = (EditText)findViewById(R.id.redshift_text);

        flat_check = (CheckBox)findViewById(R.id.flat_check);

        run_button = (Button)findViewById(R.id.button_run);
        clear_button = (Button)findViewById(R.id.button_clear);

        plus_button = (ImageButton)findViewById(R.id.button_plus);
        minus_button = (ImageButton)findViewById(R.id.button_minus);

        redshift_input.setText(String.valueOf(Redshift));

        CosmoModel = new CosmoCalc();

        connectCheckBox();
        initializeBars();
        setButtons();
    }

    protected void connectCheckBox() {
        flat_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (FlatUniverse = b) {
                    lambda_bar.setEnabled(false);
                    lambda_bar.setProgress(100 - matter_bar.getProgress());
                    lambda_text.setText(convertOmegaLambda(lambda_bar.getProgress()));
                    OmegaLambda = 1.0 - OmegaMatter;
                }
                else {
                    lambda_bar.setEnabled(true);
                }
            }
        });
    }

    protected void initializeBars() {

        hubble_bar.setMax(400); // Hubble constant goes from 60.0 to 100.0
        matter_bar.setMax(100); // Omega matter goes from 0.00 to 1.00
        lambda_bar.setMax(100); // Omega matter goes from 0.00 to 1.00

        /** CONNECT THE SEEKBAR: HUBBLE CONSTANT **/
        hubble_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                hubble_text.setText(convertHubbleConstant(hubble_bar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                HubbleConstant = ((double)seekBar.getProgress() + 600.0) * 0.1;
            }
        });

        /** CONNECT THE SEEKBAR: OMEGA MATTER **/
        matter_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    matter_text.setText(convertOmegaMatter(matter_bar.getProgress()));
                    if (FlatUniverse) {
                        lambda_bar.setProgress(100 - matter_bar.getProgress());
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                OmegaMatter = (double)seekBar.getProgress() * 0.01;
                if (FlatUniverse) {
                    OmegaLambda = 1.0 - OmegaMatter;
                }
            }
        });

        /** CONNECT THE SEEKBAR: OMEGA MATTER **/
        lambda_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if ((b && !FlatUniverse) || FlatUniverse) {
                    lambda_text.setText(convertOmegaLambda(lambda_bar.getProgress()));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                OmegaLambda = (double)seekBar.getProgress() * 0.01;
            }
        });

        /** SET THE DEFAULT MODEL FOR CONVENIENCE **/
        hubble_bar.setProgress((int)(HubbleConstant*10.0 - 600.0));
        matter_bar.setProgress((int)(OmegaMatter * 100.0));
        lambda_bar.setProgress((int)(OmegaLambda * 100.0));

        hubble_text.setText(convertHubbleConstant(hubble_bar.getProgress()));
        matter_text.setText(convertOmegaMatter(matter_bar.getProgress()));
        lambda_text.setText(convertOmegaLambda(lambda_bar.getProgress()));
    }

    protected void setButtons() {

        /** SET RUN BUTTON LISTENER **/
        run_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runModel();
            }
        });

        /** SET CLEAR BUTTON LISTENER **/
        clear_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFields();
            }
        });

        /** SET BUTTON TO INCREASE H0 **/
        plus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hubble_bar.setProgress((hubble_bar.getProgress() < hubble_bar.getMax()) ? hubble_bar.getProgress() + 1 : hubble_bar.getMax());
                HubbleConstant = ((double)hubble_bar.getProgress() + 600.0) * 0.1;
            }
        });

        /** SET BUTTON TO DECREASE H0 **/
        minus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hubble_bar.setProgress((hubble_bar.getProgress() > 0) ? hubble_bar.getProgress() - 1 : 0);
                HubbleConstant = ((double)hubble_bar.getProgress() + 600.0) * 0.1;
            }
        });
    }

    protected void runModel() {
        String tmp;
        double x;

        tmp = redshift_input.getText().toString();
        Redshift = Double.parseDouble(tmp);

        /** SET/UPDATE COSMOLOGICAL MODEL **/
        CosmoModel.setCosmology(HubbleConstant, OmegaMatter, OmegaLambda);

        /** CALCULATE THE MODEL **/
        CosmoModel.calculate_model(Redshift);

        /** UPDATE THE VIEWS **/
        x = CosmoModel.getAgeUniverse();
        if (x < 1.0) {
            x *= 1000.0;
            tmp = getString(R.string.age_universe);
            tmp = tmp.substring(0, tmp.length()-6) + "(Myr): " + String.format("%.2f", x);
        }
        else {
            tmp = getString(R.string.age_universe) + " " + String.format("%.3f", x);
        }
        ((TextView)findViewById(R.id.age_universe_text)).setText(tmp);

        x = CosmoModel.getAgeAtZ();
        if (x < 1.0) {
            x *= 1000.0;
            tmp = getString(R.string.age_universe_at);
            tmp = tmp.substring(0, tmp.length()-6) + "(Myr): " + String.format("%.2f", x);
        }
        else {
            tmp = getString(R.string.age_universe_at) + " " + String.format("%.3f", x);
        }
        ((TextView)findViewById(R.id.age_universe_at_text)).setText(tmp);

        x = CosmoModel.getLookBack();
        if (x < 1.0) {
            x *= 1000.0;
            tmp = getString(R.string.look_back_time);
            tmp = tmp.substring(0, tmp.length()-6) + "(Myr): " + String.format("%.2f", x);
        }
        else {
            tmp = getString(R.string.look_back_time) + " " + String.format("%.3f", x);
        }
        ((TextView)findViewById(R.id.lookback_text)).setText(tmp);

        x = CosmoModel.getComovDist();
        if (x > 1000.0) {
            x *= 1.0e-3;
            tmp = getString(R.string.comoving_distance);
            tmp = tmp.substring(0, tmp.length()-6) + "(Gpc): " + String.format("%.2f", x);
        }
        else {
            tmp = getString(R.string.comoving_distance) + " " + String.format("%.3f", x);
        }
        ((TextView)findViewById(R.id.comdist_text)).setText(tmp);

        x = CosmoModel.getLumDist();
        if (x > 1000.0) {
            x *= 1.0e-3;
            tmp = getString(R.string.luminosity_distance);
            tmp = tmp.substring(0, tmp.length()-6) + "(Gpc): " + String.format("%.2f", x);
        }
        else {
            tmp = getString(R.string.luminosity_distance) + " " + String.format("%.3f", x);
        }
        ((TextView)findViewById(R.id.lumdist_text)).setText(tmp);

        x = CosmoModel.getAngDist();
        if (x > 1000.0) {
            x *= 1.0e-3;
            tmp = getString(R.string.angular_distance);
            tmp = tmp.substring(0, tmp.length()-6) + "(Gpc): " + String.format("%.2f", x);
        }
        else {
            tmp = getString(R.string.angular_distance) + " " + String.format("%.3f", x);
        }
        ((TextView)findViewById(R.id.angdist_text)).setText(tmp);

        x = CosmoModel.getAngScale();
        tmp = getString(R.string.angular_scale) + " " + String.format("%.3f", x);
        ((TextView)findViewById(R.id.angscale_text)).setText(tmp);
    }

    protected void resetFields() {

        /** RESET PARAMETERS **/
        OmegaMatter = 0.3;
        OmegaLambda = 0.7;
        HubbleConstant = 69.0;
        Redshift = 2.0;
        FlatUniverse = false;

        hubble_bar.setProgress((int)(HubbleConstant*10.0 - 600.0));
        matter_bar.setProgress((int)(OmegaMatter * 100.0));
        lambda_bar.setProgress((int)(OmegaLambda * 100.0));

        hubble_text.setText(convertHubbleConstant(hubble_bar.getProgress()));
        matter_text.setText(convertOmegaMatter(matter_bar.getProgress()));
        lambda_text.setText(convertOmegaLambda(lambda_bar.getProgress()));

        flat_check.setChecked(FlatUniverse);
        redshift_input.setText(String.valueOf(Redshift));

        /** RESET RESULTS **/
        ((TextView)findViewById(R.id.age_universe_text)).setText(R.string.age_universe);
        ((TextView)findViewById(R.id.age_universe_at_text)).setText(R.string.age_universe_at);
        ((TextView)findViewById(R.id.lookback_text)).setText(R.string.look_back_time);
        ((TextView)findViewById(R.id.comdist_text)).setText(R.string.comoving_distance);
        ((TextView)findViewById(R.id.lumdist_text)).setText(R.string.luminosity_distance);
        ((TextView)findViewById(R.id.angdist_text)).setText(R.string.angular_distance);
        ((TextView)findViewById(R.id.angscale_text)).setText(R.string.angular_scale);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putDouble("H0", HubbleConstant);
        outState.putDouble("OMEGA_MATTER", OmegaMatter);
        outState.putDouble("OMEGA_LAMBDA", OmegaLambda);
        outState.putDouble("Z", Redshift);
        outState.putBoolean("FLAT_CHECK", FlatUniverse);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        HubbleConstant = savedInstanceState.getDouble("H0");
        OmegaMatter = savedInstanceState.getDouble("OMEGA_MATTER");
        OmegaLambda = savedInstanceState.getDouble("OMEGA_LAMBDA");
        Redshift = savedInstanceState.getDouble("Z");
        FlatUniverse = savedInstanceState.getBoolean("FLAT_CHECK");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        hubble_bar.setProgress((int)(HubbleConstant*10.0 - 600.0));
        matter_bar.setProgress((int)(OmegaMatter * 100.0));
        lambda_bar.setProgress((int)(OmegaLambda * 100.0));
        flat_check.setChecked(FlatUniverse);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cosmo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.help_menu:
                showHelp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showHelp() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    /** METHODS FOR CONVERSION **/
    protected android.text.Spanned convertHubbleConstant(int value) {
        double x = ((double)value + 600.0) * 0.1;
        return Html.fromHtml(String.format("H<sub><small>0</small></sub> = %.1f<br>km/s/Mpc", x));
    }

    protected android.text.Spanned convertOmegaMatter(int value) {
        double x = (double)value * 0.01;
        return Html.fromHtml(String.format("\u03A9<sub><small>m</small></sub> = %.2f", x));
    }

    protected android.text.Spanned convertOmegaLambda(int value) {
        double x = (double)value * 0.01;
        return Html.fromHtml(String.format("\u03A9<sub><small>\u039B</small></sub> = %.2f", x));
    }
}
