package com.fiacconi.davide.cosmologycalculator;

import java.lang.Math;

public class CosmoCalc {

    private double h;
    private double H0;
    private double OmegaM;
    private double OmegaL;
    private double OmegaK;
    private double OmegaR;
    private double Z;

    private double AgeUniverse;
    private double Age_at_z;
    private double Distance;

    public CosmoCalc() {}

    public void setCosmology(double h0, double om, double ol) {
        h = h0 / 100.0;
        H0 = h0 * 0.0010220146678031027; // Transform H0 in 1/Gyr
        OmegaM = om;
        OmegaL = ol;
        OmegaR = 4.18343e-5 / h / h; // I consider a tiny bit of radiation as from Planck
        OmegaK = 1. - OmegaM - OmegaL;
    }

    public void calculate_model(double z) {
        int NSteps = 1000; // Empirically tested that it is good enough and not too slow
        double a_age, a_time, a_dist;
        double t_age, t_time, t_dist;
        double deriv_age, deriv_time, deriv_dist;
        double delta_age, delta_time, delta_dist;

        /** Set control values for Age_at_z and Distance **/
        Age_at_z = -1.;
        Distance = -1.;

        /** If during the radiation epoch, we use analytical formula **/
        if (1.+z > 1.0e5) {
            Age_at_z = 0.5 / Math.sqrt(OmegaR) / Math.pow(1. + z, 2) / H0;
        }

        /** If very nearby, we use analytical formula **/
        if ((a_dist = 1./(1.+z)) > 0.999) {
            Distance = 2.99e3 * distance_integrand(a_dist) * (a_dist - 1.) / h;
        }

        /** Init variables for integration **/
        delta_age = -Math.log(1.0e-5) / NSteps;
        delta_time = (-Math.log(1. + z) - Math.log(1.0e-5)) / NSteps;
        delta_dist = (-Math.log(1. + z) - Math.log(0.999)) / NSteps;

        a_age = a_time = 1.0e-5;
        a_dist = 0.999;
        t_age = t_time = a_age * a_age * 0.5 / Math.sqrt(OmegaR);
        t_dist = distance_integrand(a_dist) * (a_dist - 1.);

        /** Integration loop **/
        for (int i=0; i<NSteps; ++i) {
            deriv_age = age_integrand(a_age);
            a_age *= Math.exp(delta_age);
            t_age += 0.5 * (age_integrand(a_age) + deriv_age) * delta_age;

            /** Do the actual calculation for Age_at_z only if analytic formula has not been used **/
            if (Age_at_z < 0.0) {
                deriv_time = age_integrand(a_time);
                a_time *= Math.exp(delta_time);
                t_time += 0.5 * (age_integrand(a_time) + deriv_time) * delta_time;
            }

            /** Do the actual calculation for Distance only if analytic formula has not been used **/
            if (Distance < 0.0) {
                deriv_dist = distance_integrand(a_dist);
                a_dist *= Math.exp(delta_dist);
                t_dist += 0.5 * (distance_integrand(a_dist) + deriv_dist) * delta_dist;
            }
        }
        AgeUniverse = t_age / H0;
        if (Age_at_z < 0.0) Age_at_z = t_time / H0;
        if (Distance < 0.0) Distance = t_dist * 2.99e3 / h;
        Z = z;
    }

    public double getAgeUniverse() {
        return Math.abs(AgeUniverse);
    }

    public double getAgeAtZ() {
        return Math.abs(Age_at_z);
    }

    public double getLookBack() {
        return Math.abs(AgeUniverse - Age_at_z);
    }

    public double getComovDist() {
        return Math.abs(Distance);
    }

    public double getLumDist() {
        return (1. + Z) * getTransverseDist();
    }

    public double getAngDist() {
        return getTransverseDist() / (1. + Z);
    }

    public double getAngScale() {
        return getAngDist() * 0.004848137;
    }

    /** PRIVATE METHODS **/
    private double age_integrand(double a) {
        return Math.pow(a, 1.5) / Math.sqrt(OmegaM + OmegaL*a*a*a + OmegaK*a*a + OmegaR / a);
    }

    private double distance_integrand(double a) {
        return -Math.sqrt(a / (OmegaM + OmegaL*a*a*a + OmegaK*a*a + OmegaR / a));
    }

    private double getTransverseDist() {
        double dh = 2.99e3 / h;
        double dc = Math.abs(Distance) / dh;

        if (OmegaK > 0.0) {
            return dh * Math.sinh(Math.sqrt(OmegaK) * dc) / Math.sqrt(OmegaK);
        }
        else if (OmegaK < 0.0) {
            return dh * Math.sin(Math.sqrt(Math.abs(OmegaK)) * dc) / Math.sqrt(Math.abs(OmegaK));
        }
        else {
            return dh * dc;
        }
    }
}

